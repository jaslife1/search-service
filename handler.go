package main

import (
	"context"
	"log"

	"github.com/micro/go-micro/metadata"
	pb "gitlab.com/lazybasterds/alpaca/search-service/proto/search"
)

type service struct {
	db *DBConnection
}

func (s *service) GetRepo() Repository {
	return &SearchRepository{s.db}
}

func (s *service) Search(ctx context.Context, req *pb.Query, res *pb.NodeResponse) error {
	log.Println(ctx)
	repo := s.GetRepo()
	defer repo.Close(ctx)

	mdata, ok := metadata.FromContext(ctx)
	var requestID string
	if ok {
		requestID = mdata["requestid"]
	}
	query := req.Query
	nodes, err := repo.SearchFullText(ctx, query)
	if err != nil {
		log.Printf("Error: Failed to get results for %s : %+v", query, err)
		return err
	}
	log.Printf("Method: Search, RequestID: %s, Return: %+v", requestID, nodes)
	res.Nodes = nodes
	return nil
}

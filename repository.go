package main

import (
	"context"

	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/mongo"

	pb "gitlab.com/lazybasterds/alpaca/search-service/proto/search"
)

const (
	dbName             = "alpacadb"
	nodeTypeCollection = "nodetype"
)

//Repository is an interface for getting information from the database.
type Repository interface {
	SearchFullText(ctx context.Context, query string) ([]*pb.Node, error)
	Close(ctx context.Context)
}

//SearchRepository concrete implementation of the Repository interface.
type SearchRepository struct {
	db *DBConnection
}

//SearchFullText will search for the Node that contains the query string specified
//This uses the Text index of MongoDB so this is limited by matching the full word.
//This function will return a list of nodes found based on the query string if there are no errors.
//Otherwise it will return nil and the error.
func (repo *SearchRepository) SearchFullText(ctx context.Context, query string) ([]*pb.Node, error) {
	collection := repo.collection(nodeTypeCollection)
	cur, err := collection.Find(ctx, bson.M{
		"$text": bson.M{
			"$search": query,
		},
	})

	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	var nodes []Node

	for cur.Next(ctx) {

		var node Node
		err := cur.Decode(&node)
		if err != nil {
			return nil, err
		}

		nodes = append(nodes, node)
	}
	if err := cur.Err(); err != nil {
		return nil, err
	}

	results := make([]*pb.Node, 0, len(nodes))
	for _, node := range nodes {
		results = append(results, &pb.Node{
			Id:   node.ID.Hex(),
			Name: node.Name,
			Type: node.Type,
			Coordinate: &pb.Coordinate{
				X: node.Coordinate.X,
				Y: node.Coordinate.Y,
				Z: node.Coordinate.Z,
			},
			FloorID: node.FloorID,
			Details: &pb.Details{
				Description: node.Details.Description,
				Logoid:      node.Details.LogoID.Hex(),
				Imageid:     node.Details.ImageID.Hex(),
				Location:    node.Details.Location,
				Category:    node.Details.Category,
				Tags:        node.Details.Tags,
				OpeningHours: &pb.OpeningHours{
					Sunday:    node.Details.OpeningHours.Sunday,
					Monday:    node.Details.OpeningHours.Monday,
					Tuesday:   node.Details.OpeningHours.Tuesday,
					Wednesday: node.Details.OpeningHours.Wednesday,
					Thursday:  node.Details.OpeningHours.Thursday,
					Friday:    node.Details.OpeningHours.Friday,
					Saturday:  node.Details.OpeningHours.Saturday,
				},
			},
		})
	}

	return results, nil
}

//Close closes the session
func (repo *SearchRepository) Close(ctx context.Context) {
	// TODO: Revisit if this is the correct context when closing session
	repo.db.session.EndSession(ctx)
}

func (repo *SearchRepository) collection(coll string) *mongo.Collection {
	return repo.db.client.Database(dbName).Collection(coll)
}
